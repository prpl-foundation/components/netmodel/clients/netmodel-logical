# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.2 - 2022-11-28(15:34:53 +0000)

### Other

- [NetModel] Update copyright information

## Release v1.0.1 - 2022-07-04(15:48:50 +0000)

### Other

- Opensource component

## Release v1.0.0 - 2022-02-28(10:53:27 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.1.0 - 2022-02-04(07:15:20 +0000)

### New

- create netmodel client for tr181-logical

